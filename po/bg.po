# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Атанас Ковачки <crashdeburn@gmail.com>, 2014
# Gabriel Radev <gabosss@gmail.com>, 2015
# Ivo, 2017
# Kaloyan Nikolov <kotipuka01@gmail.com>, 2016
# Tihomir Hristov <inactive+Tihomir@transifex.com>, 2014
# 4Joy <kiril.banialiev@gmail.com>, 2015
# Kiril Ivailov Velinov <kirilvelinov@gmail.com>, 2012
# alexdimitrov <kvikmen@gmail.com>, 2013
# Petya Antonova <gothfield@gmail.com>, 2014
# Yani Gerov <alexander.waterdale@gmail.com>, 2013
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: Tails developers <tails@boum.org>\n"
"POT-Creation-Date: 2019-10-21 09:06+0200\n"
"PO-Revision-Date: 2018-04-12 18:58+0000\n"
"Last-Translator: IDRASSI Mounir <mounir.idrassi@idrix.fr>\n"
"Language-Team: Bulgarian (http://www.transifex.com/otf/torproject/language/"
"bg/)\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../lib/Tails/Persistence/Setup.pm:265
msgid "Setup Tails persistent volume"
msgstr "Настройка Tails устойчиви обем"

#: ../lib/Tails/Persistence/Setup.pm:343 ../lib/Tails/Persistence/Setup.pm:481
msgid "Error"
msgstr "ГРЕШКА"

#: ../lib/Tails/Persistence/Setup.pm:372
#, perl-format
msgid "Device %s already has a persistent volume."
msgstr "Устройството %s вече има постоянен обем."

#: ../lib/Tails/Persistence/Setup.pm:380
#, perl-format
msgid "Device %s has not enough unallocated space."
msgstr "Устройството %s не е достатъчно неразпределено пространство."

#: ../lib/Tails/Persistence/Setup.pm:387 ../lib/Tails/Persistence/Setup.pm:401
#, perl-format
msgid "Device %s has no persistent volume."
msgstr "Устройството %s не разполага с постоянен обем."

#: ../lib/Tails/Persistence/Setup.pm:393
#, fuzzy, perl-format
msgid ""
"Cannot delete the persistent volume on %s while in use. You should restart "
"Tails without persistence."
msgstr ""
"Не можете да изтриете упорит обем по време на използване. Трябва да "
"рестартирате Tails без постоянство."

#: ../lib/Tails/Persistence/Setup.pm:407
#, fuzzy, perl-format
msgid "Persistence volume on %s is not unlocked."
msgstr "Устойчивото пространство не отключено."

#: ../lib/Tails/Persistence/Setup.pm:412
#, fuzzy, perl-format
msgid "Persistence volume on %s is not mounted."
msgstr "Устойчивото пространство не е монтирано."

#: ../lib/Tails/Persistence/Setup.pm:417
#, fuzzy, perl-format
msgid ""
"Persistence volume on %s is not readable. Permissions or ownership problems?"
msgstr ""
"Устойчивото пространство не се чете. Проблеми с разрешения или "
"собствеността ?"

#: ../lib/Tails/Persistence/Setup.pm:422
#, fuzzy, perl-format
msgid "Persistence volume on %s is not writable."
msgstr "Устойчивото пространство не е монтирано."

#: ../lib/Tails/Persistence/Setup.pm:431
#, perl-format
msgid "Tails is running from non-USB / non-SDIO device %s."
msgstr "Tails работи от устройство различно от USB/SDIO %s."

#: ../lib/Tails/Persistence/Setup.pm:437
#, perl-format
msgid "Device %s is optical."
msgstr "Устройството %s е оптично."

#: ../lib/Tails/Persistence/Setup.pm:444
#, fuzzy, perl-format
msgid "Device %s was not created using a USB image or Tails Installer."
msgstr "Устройството %s не бе създадено,чрез Tails инсталатор."

#: ../lib/Tails/Persistence/Setup.pm:688
msgid "Persistence wizard - Finished"
msgstr "Устойчивото Wizard - Завършен"

#: ../lib/Tails/Persistence/Setup.pm:691
msgid ""
"Any changes you have made will only take effect after restarting Tails.\n"
"\n"
"You may now close this application."
msgstr ""
"Всички промени, които сте направили, ще влязат в сила след рестартиране на "
"Tails. ⏎ ⏎ Сега можете да затворите тази програма."

#: ../lib/Tails/Persistence/Configuration/Setting.pm:113
msgid "Custom"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:55
msgid "Personal Data"
msgstr "Лични данни"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:57
msgid "Keep files stored in the `Persistent' directory"
msgstr "Съхранявай файловете в \"Постоянна\"(Persistent) директория"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:70
#, fuzzy
msgid "Browser Bookmarks"
msgstr "Отметки на браузъра"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:72
msgid "Bookmarks saved in the Tor Browser"
msgstr "Отметките са запазени в Tor Browser"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:85
msgid "Network Connections"
msgstr "Мрежовите връзки"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:87
msgid "Configuration of network devices and connections"
msgstr "Конфигуриране на мрежови устройства и връзки"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:100
msgid "Additional Software"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:102
msgid "Software installed when starting Tails"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:120
msgid "Printers"
msgstr "Принтери"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:122
msgid "Printers configuration"
msgstr "Конфигурация на принтери"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:135
msgid "Thunderbird"
msgstr "Thunderbird"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:137
msgid "Thunderbird emails, feeds, and settings"
msgstr ""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:150
msgid "GnuPG"
msgstr "GnuPG"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:152
msgid "GnuPG keyrings and configuration"
msgstr "GnuPG ключодържатели и конфигурация"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:165
#, fuzzy
msgid "Bitcoin Client"
msgstr "Битикоинт клиент"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:167
msgid "Electrum's bitcoin wallet and configuration"
msgstr "Електрум биткоинт портфейл и конфигурация"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:180
msgid "Pidgin"
msgstr "Pidgin"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:182
msgid "Pidgin profiles and OTR keyring"
msgstr "Pidgin профили и OTR ключодържател"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:195
msgid "SSH Client"
msgstr "SSH програма"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:197
msgid "SSH keys, configuration and known hosts"
msgstr "SSH ключове, конфигурация и познатите хостове"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:210
msgid "Dotfiles"
msgstr "Dotfiles"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:212
msgid ""
"Symlink into $HOME every file or directory found in the `dotfiles' directory"
msgstr ""
"Символична връзка в $ HOME всеки файл или директория в директория \"dotfiles"
"\""

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:96
msgid "Persistence wizard - Persistent volume creation"
msgstr "Persistence Wizard - създаване на Постоянно пространство"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:99
msgid "Choose a passphrase to protect the persistent volume"
msgstr "Изберете парола за защита на устойчивото пространство"

#. TRANSLATORS: size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:103
#, perl-format
msgid ""
"A %s persistent volume will be created on the <b>%s %s</b> device. Data on "
"this volume will be stored in an encrypted form protected by a passphrase."
msgstr ""
"Устойчивият дял %s ще бъде създаден на устройството <b>%s %s</b>. "
"Информацията на този дял ще бъде криптирана и защитена чрез парола."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:108
msgid "Create"
msgstr "Създаване на"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:151
#, fuzzy
msgid ""
"<b>Beware!</b> Using persistence has consequences that must be well "
"understood. Tails can't help you if you use it wrong! See the <i>Encrypted "
"persistence</i> page of the Tails documentation to learn more."
msgstr ""
"<b> Пазете се! </b> Използването на постоянство има последици, които трябва "
"да бъдат добре разбрани. Tails не може да ви помогне, ако се използват "
"неправилно! Вижте <a href='file:///usr/share/doc/tails/website/doc/"
"first_steps/persistence.en.html'> Tails документация за постоянство </a>, за "
"да научите повече."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:179
msgid "Passphrase:"
msgstr "Тайна фраза:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:187
msgid "Verify Passphrase:"
msgstr "Потвърди тайната фраза:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:198
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:266
msgid "Passphrase can't be empty"
msgstr "Тайната фраза не може да бъде празна"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:233
#, fuzzy
msgid "Show Passphrase"
msgstr "Тайна фраза:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:257
msgid "Passphrases do not match"
msgstr "Тайните фрази не съвпадат"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:312
#: ../lib/Tails/Persistence/Step/Delete.pm:103
#: ../lib/Tails/Persistence/Step/Configure.pm:181
msgid "Failed"
msgstr "Неуспешно"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:320
msgid "Mounting Tails persistence partition."
msgstr "Монтиране Tails постоянствен дял."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:323
msgid "The Tails persistence partition will be mounted."
msgstr "Постоянствен Tails дял ще бъде монтиран."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:332
msgid "Correcting permissions of the persistent volume."
msgstr "Коригиране на привилегии на постоянния дял."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:335
msgid "The permissions of the persistent volume will be corrected."
msgstr "Правата на устойчивото пространство ще бъдат променени."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:343
#, fuzzy
msgid "Creating default persistence configuration."
msgstr "Запазване на постоянните конфигурация"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:346
#, fuzzy
msgid "The default persistence configuration will be created."
msgstr "Постоянствен Tails дял ще бъде монтиран."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:361
msgid "Creating..."
msgstr "Създава се ..."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:364
msgid "Creating the persistent volume..."
msgstr "Създаване на устойчивото пространство ..."

#: ../lib/Tails/Persistence/Step/Delete.pm:53
msgid "Persistence wizard - Persistent volume deletion"
msgstr "Persistence съветника - устойчиви обем заличаване"

#: ../lib/Tails/Persistence/Step/Delete.pm:56
msgid "Your persistent data will be deleted."
msgstr "Вашите постоянни данни ще бъдат изтрити."

#: ../lib/Tails/Persistence/Step/Delete.pm:60
#, perl-format
msgid ""
"The persistent volume %s (%s), on the <b>%s %s</b> device, will be deleted."
msgstr ""
"Продължаващата памет %s (%s), на <b>%s %s</b> устройство, ще бъдат изтрита."

#: ../lib/Tails/Persistence/Step/Delete.pm:66
msgid "Delete"
msgstr "Изтриване"

#: ../lib/Tails/Persistence/Step/Delete.pm:117
msgid "Deleting..."
msgstr "Изтрива се ..."

#: ../lib/Tails/Persistence/Step/Delete.pm:120
msgid "Deleting the persistent volume..."
msgstr "Изтриването на постоянен обем ..."

#: ../lib/Tails/Persistence/Step/Configure.pm:88
msgid "Persistence wizard - Persistent volume configuration"
msgstr "Persistence съветника - устойчиви пространство конфигурация"

#: ../lib/Tails/Persistence/Step/Configure.pm:91
msgid "Specify the files that will be saved in the persistent volume"
msgstr ""
"Посочете файловете, които ще бъдат съхранени в продължаващото пространство"

#. TRANSLATORS: partition, size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Configure.pm:95
#, perl-format
msgid ""
"The selected files will be stored in the encrypted partition %s (%s), on the "
"<b>%s %s</b> device."
msgstr ""
"Избраните файлове ще се съхраняват в криптиран дял %s (%s), на <b>%s %s</b> "
"устройство."

#: ../lib/Tails/Persistence/Step/Configure.pm:101
msgid "Save"
msgstr "Запиши"

#: ../lib/Tails/Persistence/Step/Configure.pm:195
msgid "Saving..."
msgstr "Запазване..."

#: ../lib/Tails/Persistence/Step/Configure.pm:198
msgid "Saving persistence configuration..."
msgstr "Запазване на постоянните конфигурация"

#~ msgid "Thunderbird profiles and locally stored email"
#~ msgstr "Thunderbird профили и локално запазени имейли"

#~ msgid "GNOME Keyring"
#~ msgstr "GNOME ключодържател"

#~ msgid "Secrets stored by GNOME Keyring"
#~ msgstr "Тайните, съхранявани от GNOME ключодържател"

#~ msgid "APT Packages"
#~ msgstr "APT пакети"

#~ msgid "Packages downloaded by APT"
#~ msgstr "Пакети свален от APT"

#~ msgid "APT Lists"
#~ msgstr "APT списъци"

#~ msgid "Lists downloaded by APT"
#~ msgstr "Списъци свалени с APT"

#~ msgid "Persistence volume is not writable. Maybe it was mounted read-only?"
#~ msgstr ""
#~ "Устойчивото пространство не е записваем. Може би това е монтирана само за "
#~ "четене?"
